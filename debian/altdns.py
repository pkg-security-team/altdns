#!/usr/bin/env python3

import sys
import os

sys.path.insert(0, '/usr/share/')

from altdns.__main__ import main

if __name__ == '__main__':
    main()
